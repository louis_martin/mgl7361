#
# Appel local
#

from bidule import Bidule
from communs import chronometrer


def main():
    n = 8_000_000
    bidule = Bidule()
    resultat = executer(bidule, n)
    print('Valeur du résultat à la fin :', resultat)


@chronometrer
def executer(bidule, n):
    for i in range(n):
        bidule.incrementer()
    return bidule.get_valeur()


if __name__ == '__main__':
    main()
