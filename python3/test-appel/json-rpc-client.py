#
# Client JSON-RPC
#

import jsonrpcclient
import logging

from communs import chronometrer


def main():
    n = 1000
    url = 'http://127.0.0.1:8080/'
    # print(logging.Logger.manager.loggerDict)
    logging.getLogger('jsonrpcclient.client.request').setLevel(logging.ERROR)
    logging.getLogger('jsonrpcclient.client.response').setLevel(logging.ERROR)
    resultat = executer(url, n)
    print('Valeur du résultat à la fin :', resultat)


@chronometrer
def executer(url, n):
    for i in range(n):
        jsonrpcclient.request(url, 'incrementer')
    resultat = jsonrpcclient.request(url, 'get_bidule')
    return resultat['compteur']


if __name__ == '__main__':
    main()
