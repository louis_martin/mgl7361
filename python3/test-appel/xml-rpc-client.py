#
# Client RPC
#

import xmlrpc.client

from communs import chronometrer


def main():
    n = 1000
    bidule = xmlrpc.client.ServerProxy('http://127.0.0.1:8080')
    # print(bidule.system.listMethods())
    resultat = executer(bidule, n)
    print('Valeur du résultat à la fin :', resultat)


@chronometrer
def executer(bidule, n):
    for i in range(n):
        bidule.incrementer()
    return bidule.get_valeur()


if __name__ == '__main__':
    main()
