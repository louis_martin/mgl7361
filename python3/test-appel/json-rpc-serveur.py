#
# Serveur JSON-RPC
#

import cherrypy
from jsonrpcserver import methods, config

from bidule import Bidule


def main():
    setUpBidule()
    config.log_requests = False
    config.log_responses = False
    config_cherrypy = {
        'global' : {
            'server.socket_host' : '127.0.0.1',
            'server.socket_port' : 8080,
            'server.thread_pool' : 8,
            # Doing it explicity isn't a recommended way
            'log.screen' : False,
        }
    }
    cherrypy.quickstart(Application(), '/', config_cherrypy)


def setUpBidule():
    bidule = Bidule()

    @methods.add
    def get_bidule():
        return bidule.__dict__

    @methods.add
    def incrementer():
        bidule.incrementer()


class Application(object):
    @cherrypy.expose
    @cherrypy.tools.allow(methods=['POST'])
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def index(self):
        requete = cherrypy.request.json
        reponse = methods.dispatch(requete)
        return reponse


if __name__ == '__main__':
    main()
