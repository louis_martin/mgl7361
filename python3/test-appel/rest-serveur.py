#
# Serveur REST
#

from bottle import Bottle
import cherrypy

from bidule import Bidule


def main():
    application = construireApplication()
    cherrypy.tree.graft(application, '/')
    cherrypy.engine.start()
    cherrypy.engine.block()


def construireApplication():
    app = Bottle()
    bidule = Bidule()

    @app.route('/bidule')
    def get_bidule():
        return bidule.__dict__

    @app.route('/bidule/incrementeur', method='POST')
    def incrementer():
        bidule.incrementer()
        return

    return app


if __name__ == '__main__':
    main()
