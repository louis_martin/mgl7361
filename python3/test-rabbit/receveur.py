import pika
import time

from amqp_config import getParams


def main():

    try:

        connection = pika.BlockingConnection(getParams())

        channel = connection.channel()

        # Idempotent
        channel.queue_declare(queue='pile_travail', durable=True)

        def callback(ch, method, properties, body):
            print(" [x] Message reçu : '{}'".format(body.decode('utf8')))
            time.sleep(body.count(b'.'))
            print(" [x] Travail complété")
            ch.basic_ack(delivery_tag = method.delivery_tag)

        channel.basic_qos(prefetch_count=1)

        channel.basic_consume(callback,
                              queue='pile_travail')

        print(' [*] En attente de messages. CTRL+C pour terminer.')

        channel.start_consuming()

    except KeyboardInterrupt:
        print("\nArrêt demandé. Fin de l'exécution.")
        connection.close()


if __name__ == '__main__':
    main()
