import pika

from amqp_config import getParams


def main():

    try:

        connection = pika.BlockingConnection(getParams())

        channel = connection.channel()

        # Idempotent
        channel.queue_declare(queue='bonjour')

        def callback(ch, method, properties, body):
            # body est de type : <class 'bytes'>
            print(" [x] Message reçu : '{}'".format(body.decode()))

        channel.basic_consume(callback,
                              queue='bonjour',
                              no_ack=True)

        print(' [*] En attente de messages. CTRL+C pour terminer.')

        channel.start_consuming()

    except KeyboardInterrupt:
        print("\nArrêt demandé. Fin de l'exécution.")
        connection.close()


if __name__ == '__main__':
    main()
