import pika
import os

def getUrl():
    return os.environ.get('CLOUDAMQP_URL', 'amqp://guest:guest@localhost:5672/%2f' )

def getParams():
    params = pika.URLParameters(getUrl())
    params.socket_timeout = 5
    return params
