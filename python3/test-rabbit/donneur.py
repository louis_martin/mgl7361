import pika
import sys

from amqp_config import getParams


def main():

    connection = pika.BlockingConnection(getParams())

    channel = connection.channel()

    # Idempotent
    channel.queue_declare(queue='pile_travail', durable=True)

    message = ' '.join(sys.argv[1:]) or 'Vacances !'

    channel.basic_publish(exchange='',
                          routing_key='pile_travail',
                          body=message,
                          properties=pika.BasicProperties(
                              delivery_mode = 2, # make message persistent
                          ))

    print(" [x] Envoi de '{}'".format(message))

    connection.close()


if __name__ == '__main__':
    main()
