import pika

from amqp_config import getParams


def main():

    connection = pika.BlockingConnection(getParams())

    channel = connection.channel()

    # Idempotent
    channel.queue_declare(queue='bonjour')

    message = 'Bonjour le monde !'

    channel.basic_publish(exchange='',
                          routing_key='bonjour',
                          body=message)

    print(" [x] Envoi de '{}'".format(message))

    connection.close()


if __name__ == '__main__':
    main()
