"""
    Script pour initialiser un environnement virtuel
"""


import os
import subprocess
import venv


REPERTOIRE_ENV = 'ENV'

PACKAGES = [
    'CherryPy',
    'requests',
    'bottle',
    'jsonrpcserver',
    'jsonrpcclient',
    'pika',
]


def main(cible, packages):
    """ Crée l'environnement virtuel si requis,
        ensuite installe les packages """
    if os.path.isdir(cible):
        print(f"{cible} existe déjà.")
    else:
        venv.create(cible, with_pip=True)
        print(f"{cible} vient d'être créé.")
    installer(cible, packages)


def installer(cible, packages):
    """ Installe les packages requis """
    for package in packages:
        commande = [cible + '/bin/pip3', 'install', package, '--upgrade']
        subprocess.run(commande)
        print(f"Le package {package} est installé.")


if __name__ == '__main__':
    main(REPERTOIRE_ENV, PACKAGES)
