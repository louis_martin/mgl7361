% Titre de la présentation
% Auteur
% Date

# Titre de la première section

## Titre de la première sous-section

### Loi de Demeter {.shrink}

Principe de connaissance minimale

Requiert que toute méthode __M__ d'un objet __O__ peut simplement invoquer les méthodes des types suivants d'objets :

1. __O__ lui-même
1. Les paramètres de __M__
1. Les objets que __M__ crée/instancie
1. Les objets membres de __O__

En particulier, un objet doit éviter d'invoquer des méthodes d'un membre objet retourné par une autre méthode.

-- _Tiré de Wikipédia_

## Titre de la deuxième sous-section

### Principe de substitution de Liskov

La notion de sous-type telle que définie par Liskov et Wing est fondée sur la notion de substituabilité : si __S__ est un sous-type de __T__, alors tout objet de type __T__ peut être remplacé par un objet de type __S__ sans altérer les propriétés désirables du programme concerné.

### Contre-exemple

- Une classe _Rectangle_
    - Avec deux méthodes : une pour fixer la longeur et l'autre pour fixe la largeur
- Une sous-classe _Carré_

-- _Tiré de Wikipédia_

## Titre de la troisième sous-section

### Quelle approche recommandez-vous ?

1. La classe _Étudiant_ sous-classe la classe _Personne_

1. La classe _Étudiant_ contient une variable d'instance du type _Personne_ et délègue à celle-ci, le cas échéant

# Titre de la deuxième section

## SOLID

### Exemple d'une diapositive chargée {.shrink}

- __S__ Responsabilité unique (Single Responsibility Principle)        
une classe doit avoir une et une seule responsabilité
- __O__ Ouvert/fermé (Open/closed principle)
une classe doit être ouverte à l'extension, mais fermée à la modification
- __L__ Substitution de Liskov (Liskov substitution Principle)
une instance de type T doit pouvoir être remplacée par une instance de type G, tel que G sous-type de T, sans que cela ne modifie la cohérence du programme
- __I__ Ségrégation des interfaces (Interface segregation principle)
préférer plusieurs interfaces spécifiques pour chaque client plutôt qu'une seule interface générale
- __D__ Inversion des dépendances (Dependency Inversion Principle)
il faut dépendre des abstractions, pas des implémentations

### Quelques exemples

- JDBC
- JMS
- JMX
- Journalisation
- Configuration

# Titre de la troisième section

## Thèmes

### Exemple d'une liste très longue {.allowframebreaks}

- Complexité
- Architecture
- Microservices
- API - REST
- MVC
- Tests
- Build
- Complexité
- Architecture
- Microservices
- API - REST
- MVC
- Tests
- Build
- Complexité
- Architecture
- Microservices
- API - REST
- MVC
- Tests
- Build

# Conclusion

### Dix commandements du design

Exemple de l'inclusion d'une image, dans ce cas-ci un fichier PDF.

![Commandements](images/10_commandements_design.pdf){width=100%}
