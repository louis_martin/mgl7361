#!/bin/bash

# Thèmes
#
# Bergen
# Madrid *
# AnnArbor *
# CambridgeUS *
# EastLansing *
# Pittsburgh * très dépouillé
# Rochester
#
# Avec arborescence
#
# Antibes
# Szeged * Intéressant

# {.allowframebreaks} Pour permettre la création de plus d'une diapositive pour les débordements
# {.shrink} pour forcer le texte sur une seule diapositive

# Dresden

prefixe=${1%.*}
pandoc -t beamer "$1" -f markdown -o "$prefixe".pdf --toc -V lang:fr -V theme=Szeged -H header.txt
