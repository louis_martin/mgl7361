% MGL7361 -- Principes et applications de la conception de logiciels    
  Titre du travail
% MARL00000000 Louis Martin  
  SERJ11111111 Jean Sérien  
  RATA22222222 Aimé Raté
% \today

# Liste des abréviations, sigles et acronymes {-}

MGL
  ~ Maitrise en génie logiciel

UQAM
  ~ Université du Québec à Montréal

# Introduction

\lipsum[1-3]

# Analyse

\lipsum[4-5]

|__Type__|__Pondération__|__Date de remise__|
|:----|:---:|----:|
|Remise du choix de lecture --- individuel||12 septembre 2017|
|Choix du travail d'équipe, plan et bibliographie|5 %|26 septembre 2017|
|Résumé de lecture --- individuel|15 %|17 octobre 2017|
|Présentations des équipes|15 %|21 et 28 novembre 2017|
|Rapport final du travail d'équipe|25 %|12 décembre 2017|
|Examen final --- individuel|40 %|12 décembre 2017|

Table: Exemple d'un tableau

\lipsum[6-9]

----

~~~ {#code_1 .python .numberLines startFrom="1"}
#
# Appel local
#

import time

from bidule import Bidule


def main():
    n = 8_000_000
    bidule = Bidule()
    temps_debut = time.clock()
    resultat = executer(bidule, n)
    temps_fin = time.clock()
    print('Valeur du résultat à la fin :', resultat)
    print('Durée :', temps_fin - temps_debut, 'secondes')


def executer(bidule, n):
    for i in range(n):
        bidule.incrementer()
    return bidule.get_valeur()


if __name__ == '__main__':
    main()
~~~

----

\lipsum[13-14]

# Conclusion

\lipsum[10-12]

![Exemple d'une image](images/balancing-radar-chart.png){width=100%}

\lipsum[15]

# Bibliographie {-}

ABBOTT, Martin L. & FISHER, Michael T.  
**The Art of Scalability -- Second Edition**  
Pearson Education, Inc. 2015 -- ISBN : 978-0-13-403280-1

BASS, Len & WEBER, Ingo & ZHU, Liming  
**DevOps -- A Software Architect’s Perspective**  
Pearson Education 2015 -- ISBN : 978-0-13-404984-7

\appendix

# Titre de l'annexe A

\lipsum[1-12]

## Sous-section

\lipsum[1-3]

# Exemple d'inclusion de pages PDF

\includepdf[pages=2,scale=0.70,pagecommand={},frame=true]{images/questionnaire-resultats-aut-2017.pdf}

\includepdf[pages=3,scale=0.70,pagecommand={},frame=true]{images/questionnaire-resultats-aut-2017.pdf}
